export default interface GetAllInterface<​​T> {
  getAll(): Array<T>
}
