# express-typescript starter

## Installation

```
$ git clone https://bitbucket.org/matthewtso/express-typescript
$ cd express-typescript
$ npm install
```

## Usage

```
$ npm run build
$ PORT=3000 npm start
```

## Notes

- Support for popular node.js libraries can be found under the namespace `@types`.
  Learn more at [https://github.com/types]().
- Typescript interfaces make mocking and changing implementations easy
  for controllers, storage, and external connections.
- The `webpack.config.js` configuration infers the main entry file by reading the
  `main` attribute in `package.json`.

## To Do

- Add testing with mocks
